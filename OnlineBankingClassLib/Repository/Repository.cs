﻿using Microsoft.EntityFrameworkCore;
using OnlineBankingClassLib.Entities;
using OnlineBankingClassLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineBankingClassLib.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {

        private readonly AppDbContext appDbContext;
        public DbSet<TEntity> _entity;
   
        public Repository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
            this._entity = appDbContext.Set<TEntity>();
        }

        public void Create(TEntity model)
        {
            _entity.Add(model);
        }

        public void Delete(Guid guid)
        {
            var customer = _entity.Find(guid);
            _entity.Remove(customer);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _entity;
        }

        public TEntity GetById(Guid guid)
        {
            return _entity.Find(guid);
        }

        public void Update(Guid id, UpdateViewModel model)
        {
            throw new NotImplementedException();
        }
    }
}
