﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OnlineBankingClassLib.Entities
{
    public class Customer
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public AppUser User { get; set; }

        public DateTime Birthday { get; set; }

        public Guid AccountId { get; set; }

        [ForeignKey(nameof(AccountId))]
        public Account Account { get; set; }
        public bool DefaultPassword { get; set; } = true;

    }
}
