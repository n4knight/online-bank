﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OnlineBankingClassLib.Entities
{
    public class Account
    {
        [Key]
        public Guid Id { get; set; }

        public int? UserId { get; set; }

        public int CustomerId { get; set; }

        [MinLength(10)]
        [MaxLength(10)]
        public int AccountNumber { get; set; }

        [Column(TypeName = "decimal(38,2)")]
        public decimal Balance { get; set; }
    }
}
