﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OnlineBankingClassLib.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineBankingClassLib.Middleware
{
    public static class DbExtension
    {
        public static IServiceCollection AddDBConnection (this IServiceCollection service, IConfiguration config)
        {
            service.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(config.GetConnectionString("OBConnection")));

            service.AddIdentity<AppUser, IdentityRole>(options =>
            {

                //options.SignIn.RequireConfirmedEmail = false; //defaults to false
                //options.Password.RequireDigit = true;
                //options.Password.RequiredLength = 8;
                //options.Password.RequireNonAlphanumeric = true;
                //options.Password.RequireUppercase = true;
                //options.Password.RequiredUniqueChars = 1;
                //options.Lockout.MaxFailedAccessAttempts = 4;
                //options.Lockout.AllowedForNewUsers = true;
                //options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
            }).AddEntityFrameworkStores<AppDbContext>().AddDefaultTokenProviders();

            return service;
        }
    }
}
