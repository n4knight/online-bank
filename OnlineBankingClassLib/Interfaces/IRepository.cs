﻿using OnlineBankingClassLib.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineBankingClassLib.Interfaces
{
    public interface IRepository<TEntity> where TEntity:class
    {
        void Create( TEntity model);

        TEntity GetById(Guid guid);

        void Update(Guid id ,UpdateViewModel model);

        void Delete(Guid guid);

        IEnumerable<TEntity> GetAll();

    }
}
