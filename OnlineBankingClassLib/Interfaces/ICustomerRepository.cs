﻿using OnlineBankingClassLib.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OnlineBankingClassLib.Interfaces
{
    interface ICustomerRepository: IRepository<Customer>
    {

    }
}
