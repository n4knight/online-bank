﻿ using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OnlineBankingClassLib.Interfaces.UnitOfWork
{
    public interface IUnitOfWork
    {
        public void Commit();
        public Task CommitAsync();
       
    }
}
