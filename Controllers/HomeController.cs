﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Onlinebank_rework.Models;
using OnlineBankingClassLib.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Onlinebank_rework.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly UserManager<AppUser> _userManager;

        private readonly SignInManager<AppUser> _signIn;



        public HomeController(ILogger<HomeController> logger, UserManager<AppUser> userManager, SignInManager<AppUser> signIn)
        {
            _logger = logger;

            _userManager = userManager;
            _signIn = signIn;
        }

        public IActionResult Index()
        {
            return View();
        }

        
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Register(RegistrationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();

            }
            AppUser user = new AppUser
            {
                UserName = model.UserName,
                Email = model.Email
            };
            IdentityResult result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded )
            {
                TempData["EnrollSuccess"] = "Enrollment Was Successful!";
                return RedirectToAction("Userdashboard", "Dashboard");

            }

            ModelState.AddModelError(string.Empty, "An Error Occurred!!");
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = await _userManager.FindByNameAsync(model.Username);
            if (user is null) ModelState.AddModelError(string.Empty, "An Error Occurred!!");

            var result = await _signIn.PasswordSignInAsync(model.Username, model.password, false, false);
            if (result.Succeeded)
            {
                return RedirectToAction("Userdashboard", "Dashboard");
            }

            ModelState.AddModelError(string.Empty, "An Error Occurred!!");
            return View(model);



        }

        public IActionResult Logout() => RedirectToAction("Index", "Home");

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
